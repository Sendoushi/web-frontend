'use strict'

const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const utils = require('./utils');
const config = require('./config');
const packageConfig = require('../package.json');
const isProduction = process.env.NODE_ENV === 'production';

// -------------------------------
// Export

module.exports = {
  mode: 'development',
  context: path.resolve(__dirname, '../'),
  entry: {
    app: './src/main.ts'
  },
  output: {
    path: config.assetsRoot,
    filename: '[name].js',
    publicPath: config.assetsPublicPath
  },
  resolve: {
    extensions: ['.ts', '.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': path.resolve(__dirname, '../src')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          cacheBusting: isProduction,
          transformToRequire: {
            video: ['src', 'poster'],
            source: 'src',
            img: 'src',
            image: 'xlink:href'
          }
        }
      },
      {
        test: /\.ts?$/,
        loader: 'ts-loader',
        options: {
          appendTsSuffixTo: [/\.vue$/]
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: utils.assetsPath('img/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: [':data-src']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          // creates style nodes from JS strings
          !isProduction ? 'style-loader' : MiniCssExtractPlugin.loader,
          // translates CSS into CommonJS
          {
            loader: "css-loader",
            options: {
              sourceMap: !isProduction,
              importLoaders: 1
            }
          },
          // postcss
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('autoprefixer')({ 'browsers': packageConfig.browserslist }),
                isProduction ? require('cssnano')() : null
              ].filter(val => !!val),
            }
          },
          // compiles Sass to CSS
          {
            loader: "sass-loader",
            options: {
              data: "$env: " + process.env.NODE_ENV + ";",
              sourceMap: !isProduction
            }
          }
        ]
}
    ]
  },
  node: {
    // prevent webpack from injecting useless setImmediate polyfill because Vue
    // source contains it (although only uses it if it's native).
    setImmediate: false,
    // prevent webpack from injecting mocks to Node native modules
    // that does not make sense for the client
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  },
  plugins: [
    new VueLoaderPlugin(),

    // copy custom static assets
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, '../static'),
        to: config.assetsSubDirectory,
        ignore: ['.*', 'index.html']
      }
    ])
  ]
}
