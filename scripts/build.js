'use strict'

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const rm = require('rimraf');
const path = require('path');
const chalk = require('chalk');
const webpack = require('webpack');
const config = require('./config');
const webpackConfig = require(process.env.NODE_ENV === 'production' ? './webpack.prod.conf' : './webpack.dev.conf');

console.log('building for: ' + process.env.NODE_ENV, '...');

// remove old files
rm(path.join(config.assetsRoot, config.assetsSubDirectory), err => {
  if (err) {
    throw err;
  }

  // run webpack
  webpack(webpackConfig, (err, stats) => {
    if (err) {
      throw err;
    }

    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false, // If you are using ts-loader, setting this to true will make TypeScript errors show up during build.
      chunks: false,
      chunkModules: false
    }) + '\n\n');

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'));
      process.exit(1);
    }

    console.log(chalk.cyan('  Build complete.\n'));
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ));
  });
});
