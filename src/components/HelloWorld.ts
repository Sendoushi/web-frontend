import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import tmpl from './HelloWorld.html';
import './HelloWorld.scss';

// ----------------------------------------
// Class

@Component({
  template: tmpl
})
export class HelloWorld extends Vue {
  constructor() {
    super();
  }

  protected goToPage2() {
    this.$router.push({ name: 'page2' });
  }
}
