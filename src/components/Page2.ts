import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import tmpl from './Page2.html';

// ----------------------------------------
// Class

@Component({
  template: tmpl
})
export class Page2 extends Vue {
  constructor() {
    super();
  }
}
