import Vue from 'vue';
import Router from 'vue-router';
import { HelloWorld } from '@/components/HelloWorld';
import { Page2 } from '@/components/Page2';

// ----------------------------------------
// Runtime

Vue.use(Router);

// ----------------------------------------
// Class

export default new Router({
  routes: [
    {
      path: '/',
      name: 'helloWorld',
      component: HelloWorld
    }, {
      path: '/page-2',
      name: 'page2',
      component: Page2
    }
  ]
});
