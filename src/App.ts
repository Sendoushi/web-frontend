import Vue from 'vue';
import { Component } from 'vue-property-decorator';

// ----------------------------------------
// Class

@Component({
  template: '<ion-app><router-view></router-view></ion-app>'
})
export class App extends Vue {
  constructor() {
    super();
  }
}
