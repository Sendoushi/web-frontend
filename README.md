# web-frontend

Let the awesomeness begin!

## Table of Contents

1. [Development](#development)

==========

## Development

Entry point is located at [src/main.ts](src/main.ts).

### Install

1. Install [Node.js](https://nodejs.org)
2. On the CLI, on the project folder run: 

```bash
rm -rf node_modules; \
npm install -g cordova; \
npm install; \
ionic cordova platform remove ios; \
ionic cordova platform add ios; \
ionic cordova platform remove android; \
ionic cordova platform add android; \
ionic cordova platform remove browser; \
ionic cordova platform add browser;
```

### Build Setup

``` bash
# install dependencies
npm install

# build for development
npm run build

# build for production and view the bundle analyzer report
npm run build-prod --report

# serve the build
npm run start
```
